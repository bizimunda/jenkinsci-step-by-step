# Installing Jenkins on Ubuntu 18 Using An Ansible Galaxy Role
---
## Steps:
   - Use **Vagrant** to launch two Linux Ubuntu/Bionic64 VMs (See the *Vagranfile*). 
     * The first VM acts as the Ansible Control Node. 
     * The second VM acts as the target Node on which Jenkins will be installed.
   - Configure SSH in order to enable Ansible to connect to the target VM (Use **ssh-keygen** and **ssh-copy-id** commands).
   - Add the inventory (the **hosts** file in this example)   
   - Install the **geerlingguy.jenkins** jenkins role from within _/vagrant_ folder.
       ```bash
       $ ansible-galaxy install geerlingguy.jenkins -p ./roles
       ```
   - Write the **Ansible** Playbook. An example it provided in *jenkins-provioning_role.yml*
        
     ```yaml
       ---
       - name: Install Jenkins
         hosts: jenkins
         become: true
         roles:
            - geerlingguy.jenkins
     ```      
 - Run the Ansible playbook
      ``` sh
         $ ansible-playbook -i hosts jenkins-provioning_role.yml
      ```
     