# Jenkins Decalarative Pipeline Syntax / Parallel Stages
---
Stages in Declarative Pipeline may have a **parallel** section containing a list of nested stages to be run in parallel. Note that a stage must have one and only one of steps, stages, parallel, or matrix. It is not possible to nest a parallel or matrix block within a stage directive if that stage directive is nested within a parallel or matrix block itself. However, a stage directive within a parallel or matrix block can use all other functionality of a stage, including agent, tools, when, etc.

In addition, you can force your parallel stages to all be aborted when any one of them fails, by adding `failFast` true to the stage containing the parallel. Another option for adding `failfast` is adding an option to the pipeline definition: `parallelsAlwaysFailFast()`

## Example:  Parallel Stages, Declarative Pipeline

```groovy
    stage('Parallel Stage') {
            
            failFast true
            parallel {
                stage('Branch A') {
                    agent {
                        label "for-branch-a"
                    }
                    steps {
                        echo "On Branch A"
                    }
                }
                stage('Branch B') {
                    steps {
                        echo "On Branch B"
                    }
                }
                stage('Branch C') {
                    stages {
                        stage('Nested 1') {
                            steps {
                                echo "In stage Nested 1 within Branch C"
                            }
                        }
                        stage('Nested 2') {
                            steps {
                                echo "In stage Nested 2 within Branch C"
                            }
                        }
                    }
                }
            }
	  }
```
  See the [Parallel Section in the official Jenkins documentation] (https://jenkins.io/doc/book/pipeline/syntax/#parallel) for its specific usage.