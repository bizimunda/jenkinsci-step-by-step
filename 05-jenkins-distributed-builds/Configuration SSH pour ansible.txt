Sur le noeud de controle ansible:
=================================
 1- Creer une paire de cles ssh 
    $ ssh-keygen -t rsa
 
 2-  Copier la cle vers le poste cible ssh-copy-id 
  $ ssh-copy-id  vagrant@10.0.15.31    // CETTE COMMANDE EXIGE LA CONFIGURATION CI DESSOUS
 
Sur le poste client
===================
1- $ sudo nano /etc/ssh/sshd_config

2- Change this line:
    PasswordAuthentication no
    to
    PasswordAuthentication yes
3- Restart daemon:
   $ sudo systemctl restart sshd

REF

https://www.digitalocean.com/community/questions/ssh-copy-id-not-working-permission-denied-publickey

